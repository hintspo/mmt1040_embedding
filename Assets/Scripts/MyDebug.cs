﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MyDebug : MonoBehaviour {
	
	// VARS DICTIONARY:
	private SortedDictionary<string, string> dictVars;

	// APP NAME AND VERSION:
	public string appName = "Embedding Test";
	public string appVersion = "0.0"; // keep track of version

	// SHOW ONSCREEN DEBUG INFORMATION:
	public bool debug = true; // turn onscreen debug on/off
	private string debugText; // keep debug information
	private int debugCount;
	public float debugY = 10.0f; // debug label position (Y)
	private float debugH; // debug label height


	// Use this for initialization
	void Start () {
		// Init:
		debugText = "";
		debugCount = 1;

		// Print srcValue in log:
		log ("Application.scrValue: " + Application.srcValue);

		// Find query string:
		int pos = Application.srcValue.IndexOf('?');
		if (pos != -1) {
			// ? char found in srcValue:
			string qs = Application.srcValue.Substring(pos+1);
			dictVars = parseQueryString(qs);
			
			// Iterate Dictionary:
			foreach (KeyValuePair<string,string> kvp in dictVars) {
				log ("dictVars[\"" + kvp.Key + "\"] = \"" + kvp.Value + "\"");
			}
		} else {
			log ("NO query string in Unity src");
		}

		/*
		// Test SetUnityVars function:
		string var1 = "Per Bjarne & Co.";
		string var2 = "50 + 50 = 100";
		string queryString = "var1=" + WWW.EscapeURL(var1);
		queryString += "&var2=" + WWW.EscapeURL(var2);
		queryString += "&var1=Test";
		queryString += "&4=Test4";
		queryString += "&SingleOnly1";
		queryString += "&SingleOnly2";
		SetUnityVars (queryString);
		*/

		/*
		// Test CallBrowserFunction function:
		//CallBrowserFunction ("Start");
		*/
	}


	// Update is called once per frame
	void Update () {
	
	}


	// GUI update
	void OnGUI () {
		// Print debug information:
		debugH = Screen.height - debugY - 35.0f;
		if (debug) {
			GUI.Box(new Rect(5.0f, debugY, (Screen.width-10.0f), debugH), "Debug - " + appName + " [v." + appVersion + "]");
			GUI.Label(new Rect(10.0f, debugY+20.0f, (Screen.width-20.0f), (debugH-20.0f)), debugText);
			if (GUI.Button (new Rect (5.0f, (debugY+debugH+5.0f), ((Screen.width/2)-10.0f), 20.0f), "Clear Log")) {
				clearLog();
			}
			if (GUI.Button (new Rect ((Screen.width/2)+5.0f, (debugY+debugH+5.0f), ((Screen.width/2)-10.0f), 20.0f), "SendToBrowser(debugText)")) {
				CallBrowserFunction(debugText);
			}
		}
	}


	// Called from browser (Web Player object):
	public void SetUnityVars (string iQueryString) {
		log ("** Query String: \"" + iQueryString + "\"");

		// Init Dictionary:
		dictVars = parseQueryString(iQueryString);

		// Iterate Dictionary:
		foreach (KeyValuePair<string,string> kvp in dictVars) {
			log ("dictVars[\"" + kvp.Key + "\"] = \"" + kvp.Value + "\"");
		}
	} // SetUnityVars


	// Call JS function in browser:
	private void CallBrowserFunction (string iParam) {
		Application.ExternalCall ("SendToBrowser", iParam);
	}


	private SortedDictionary<string, string> parseQueryString (string iQueryString) {
		// Construct Dictionary:
		SortedDictionary<string, string> qs = new SortedDictionary<string, string>();

		// Split query string:
		string[] allVars = iQueryString.Split('&');
		for (int i=0; i<allVars.Length; i++) {
			// Split each var:
			string[] oneVar = allVars[i].Split('=');
			if (oneVar.Length > 1) {
				// Use var name as key:
				try {
					qs.Add(WWW.UnEscapeURL(oneVar[0]), WWW.UnEscapeURL(oneVar[1]));
				} catch (System.ArgumentException) {
					// Key already exist - use last key (this one):
					qs[oneVar[0]] = oneVar[1];
				}
			} else if (oneVar.Length == 1) {
				// Value only - use array index number as key:
				try {
					qs.Add(i.ToString(), WWW.UnEscapeURL(oneVar[0]));
				} catch (System.Exception) {
					// Ignore all exceptions - e.g. key already exist
				}
			}
		} // for

		// Return Dictionary:
		return qs;
	}


	// Update debug text:
	private void log (string text) {
		// Add debug text:
		if (debug) {
			string newLine = "\n";
			if (debugCount == 1) newLine = ""; // no new line on first log element
			debugText = debugCount + ": " + text + newLine + debugText;
			debugCount++;
		}
	}


	// Clear debug text:
	private void clearLog () {
		debugText = "";
		debugCount = 1;
	}
}
